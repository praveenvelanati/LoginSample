//
//  ViewController.swift
//  autoresizing
//
//  Created by praveen velanati on 3/20/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var segmentedSelector: UISegmentedControl!
    
    @IBOutlet weak var loginEmailTF: UITextField!
    
    @IBOutlet weak var loginPwdTF: UITextField!
    
    @IBOutlet weak var loginBtn: CustomButton!
    
    @IBOutlet weak var forgotPwdBtn: CustomButton!
    
    @IBOutlet weak var loginFBBtn: CustomButton!
    
    @IBOutlet weak var loginTwtrBtn: CustomButton!
    
    
    @IBOutlet weak var signupEmailTF: UITextField!
    
    @IBOutlet weak var signupPwdTF: UITextField!
    
    
    @IBOutlet weak var reenntPwdTF: UITextField!
    
    @IBOutlet weak var signupBtn: CustomButton!
    
    @IBOutlet weak var signupFBBtn: CustomButton!
    
    @IBOutlet weak var signupTwtrBtn: CustomButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func selector(sender: UISegmentedControl) {
        
        if (segmentedSelector.selectedSegmentIndex == 0) {
            
            loginEmailTF.hidden = false
            loginPwdTF.hidden = false
            loginBtn.hidden = false
            forgotPwdBtn.hidden = false
            loginFBBtn.hidden = false
            loginTwtrBtn.hidden = false
            signupEmailTF.hidden = true
            signupPwdTF.hidden = true
            reenntPwdTF.hidden = true
            signupBtn.hidden = true
            signupFBBtn.hidden = true
            signupTwtrBtn.hidden = true
            
        } else {
            
            
            
            loginEmailTF.hidden = true
            loginPwdTF.hidden = true
            loginBtn.hidden = true
            forgotPwdBtn.hidden = true
            loginFBBtn.hidden =  true
            loginTwtrBtn.hidden = true
            signupEmailTF.hidden = false
            signupPwdTF.hidden = false
            reenntPwdTF.hidden = false
            signupBtn.hidden = false
            signupFBBtn.hidden = false
            signupTwtrBtn.hidden = false
            
            
            
            
            
            
            
            
            
        }
        
        
    }
    
    
    


}

