//
//  CustomButton.swift
//  AutoLayout
//
//  Created by praveen velanati on 3/20/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
  // customizing
    override func awakeFromNib() {
        
        layer.cornerRadius = 4.0
        layer.borderColor = UIColor(red: 157/255, green: 157/255, blue: 157/255, alpha: 0.5).CGColor
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSizeMake(0.0, 2.0)
    }
    

}
